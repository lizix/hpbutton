//
//  HPButton.m
//  Hoopster
//
//  Created by Zixuan Li on 7/15/14.
//  Copyright (c) 2014 Hoopster. All rights reserved.
//

#import "HPButton.h"

@implementation HPButton

- (id)initWithNormalColor:(UIColor *)normalColor hightlightedColor:(UIColor *)hightedColor frame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        UIImage *normalImage = [self buttonImageWithColor:normalColor];
        UIImage *highlightedImage = [self buttonImageWithColor:hightedColor];
        
        [self setBackgroundImage:normalImage forState:UIControlStateNormal];
        [self setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
        
    }
    
    return self;
    
}

- (UIImage *)buttonImageWithColor:(UIColor *)color {
    
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}

@end
