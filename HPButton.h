//
//  HPButton.h
//  Hoopster
//
//  Created by Zixuan Li on 7/15/14.
//  Copyright (c) 2014 Hoopster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HPButton : UIButton

/**
 * conveniently create a button with colors for both normal state and highlighted state
 *@param UIColor normalColor
 *@param UIColor hightedColor
 *@param CGRect frame
 *@return id
 */
- (id)initWithNormalColor:(UIColor *)normalColor hightlightedColor:(UIColor *)hightedColor frame:(CGRect)frame;

@end
