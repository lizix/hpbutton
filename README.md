HPButton
========

A convenient subclass of UIButton to quickly create button with colors for both normal state and highlighted state.

Create a colored button with initialization:
```
// create a new button with black color as background and blue color when pressed.
HPButton *button = [[HPButton alloc] initWithNormalColor:[UIColor blackColor]
                                           hightlightedColor:[UIColor blueColor]
                                                       frame:CGRectMake(100, 200, 120, 40)];
                                                      
// It's ready to rock and roll.                                                      
```

Follow-up update is coming.

Contact
==

Twitter:
https://twitter.com/RoyLi1004 
E-mail:
li1471@purdue.edu